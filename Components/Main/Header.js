import React, {Component} from 'react';
import {View, Text, StyleSheet, TouchableOpacity, TextInput, Image, Dimensions, Alert} from 'react-native';
import {connect} from 'react-redux';
import iconMenu from '../../Image/icon-menu.png';
import iconLogo from '../../Image/icon-logo.png';
import search from '../../Redux/API/search';
import {searchProduct} from '../../Redux/Reducer/CreateAction';

class Header extends Component{
    constructor(props){
        super(props);
        this.state={
            contentSearch:'',
        }
    }
    _search(){
        const {contentSearch} = this.state;
        search(contentSearch)
        .then(arrproduct => 
            this.props.searchProduct(arrproduct)
        )
        .catch(err => this._searchFail())
    }
    _searchFail(){
        Alert.alert(
            'Search',
            'Không tìm thấy sàn phẩm nào',
            [
              {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            // { cancelable: false }
          )
    }
    render(){
        return(
            <View style={styles.header}>
                <View style={styles.headerTop}>
                    <TouchableOpacity
                        onPress={this.props.onOpen}
                    >
                        <Image 
                            style={styles.image}
                            source={iconMenu}
                        />
                    </TouchableOpacity>
                    <Text style={styles.text}>Wearing a Dress</Text>
                    <TouchableOpacity
                        onPress={this.props.goSear}
                    >
                        <Image 
                            style={styles.image}
                            source={iconLogo}
                        />
                    </TouchableOpacity>
                </View>
                <View style={styles.hearderSearch}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={styles.textinput}
                        placeholder="What do you want to buy ?"
                        value={this.state.contentSearch}
                        onChangeText={(contentSearch)=>{this.setState({contentSearch})}}
                        onFocus={ this.props.goSear}
                        onSubmitEditing={this._search.bind(this)}
                    />
                </View>
            </View>
        );
    }
}
export default  connect(null,{searchProduct})(Header);
var {width, height} = Dimensions.get('window');
const styles=StyleSheet.create({
    header:{
        flex:2,
        backgroundColor: '#28b08a',
        justifyContent:'space-around',
        padding: width/25,
        paddingTop:width/30,
    },
    headerTop:{
        flex:1,
        flexDirection:'row',
        justifyContent: 'space-between',
        alignItems:'center',
    },
        image:{
            width:width/13,
            height:height/18,
        },
        text:{
            fontSize:width/23,
            color:'white',
        },
    hearderSearch:{
        flex:1,
        marginTop:width/70,
    },
        textinput:{
            backgroundColor:'white',
            fontSize:width/28,
        },
})