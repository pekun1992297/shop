import React, { Component } from 'react';
import {  StyleSheet,  Text,  View, TouchableOpacity, Dimensions, Image, FlatList} from 'react-native';
import {connect} from 'react-redux';
import getListProduct from '../../../../Redux/API/getListProduct';
import {fetchListProduct, getListProductsThunk,refreshListProduct,loadpage, setRefre} from '../../../../Redux/Reducer/CreateAction';


import back from '../../../../Image/backList.png';
import midi from '../../../../Image/midi.jpeg';


const url="http://192.168.1.111/app/images/product/";

class Product extends Component {
  constructor(props){
    super(props);
    this.state={
      page:1,
      refre: false,
    }
  }
  goDetail(product){
    this.props.navigation.navigate('PRODUCTDETAIL',{product});
  }
  goHome(){
    this.props.navigation.pop();
    this.props.refreshListProduct();
  }
  componentDidMount(){
    var {type} = this.props.navigation.state.params;
    var {page} = this.state;
    console.log(page,"page");
    this.props.getListProductsThunk(type.id,page)//dùng redux thunk
  }
  _refre() {
    var { type } = this.props.navigation.state.params;
    var { page } = this.state;
    var { refre } = this.props;
    this.setState({refre:true});
    getListProduct(type.id, page + 1)
      .then(listProduct => {
        if (listProduct.length != 0) {
          this.props.fetchListProduct(listProduct);
          this.setState({page: page+1, refre: false});
          this.props.setRefre();
        }
      })
      .catch(err => {console.log(err, "LOI"); this.setState({refre:false})})
  }//get list product dùng pull to refre
  // loadding(){
  //   var {type} = this.props.navigation.state.params;
  //   var {page} = this.state;
  //   console.log(page,"page");
  //   getListProduct(type.id, page+1)
  //   .then(res =>{
  //     if(res.length != 0){
  //       this.setState({page: page+1})
  //       this.props.fetchListProduct(res);
  //     }
  //   })
  //   .catch(err=> console.log(err,"LOI"))
  // }// get list product theo page kéo xuống từ bên dưới 0.1 sẻ hiện tiếp data
  // componentDidMount(){
  //     var {type} = this.props.navigation.state.params;
  //     var {page} = this.props;
  //     console.log(page,"page");
  //     this.props.getListProductsThunk(type.id,page)//dùng redux thunk
  //     // .then(listProduct => {this.props.fetchListProduct(listProduct)})
  //     // .catch(err => console.log(err,"LOI"));
  //   }
  //   loadding(){
  //     this.props.loadpage();
  //     var {type} = this.props.navigation.state.params;
  //     var {page} = this.props;
  //     console.log(page,"page");
  //     getListProduct(type.id, page+1)
  //     .then(res =>{
  //       if(res.length != 0){
  //         this.props.loadpage();
  //         this.props.fetchListProduct(res);
  //       }
  //     })
  //     .catch(err=> console.log(err,"LOI"))
  // }
  render() {
    const {textStyle0, textStyle1, textStyle2,color, colorStyle, container, header, body, imagebackstyle, textheaderstyle, products,imageProduct, productsImage, productsContent, productsShowDetail} = styles;
    const {type} = this.props.navigation.state.params;
    // console.log(arrProduct,"products");
    const {listProducts} = this.props;
    console.log(listProducts,"listproducts");
    const {refre} = this.state;
    return (
      <View style={container}>
        <View style={header}>
          <TouchableOpacity onPress={this.goHome.bind(this)}>
            <Image style={imagebackstyle} source={back} />
          </TouchableOpacity>
          <Text key={type.id} style={textheaderstyle}>{type.name}</Text>
          <Text></Text>
        </View>
        <View style={body}>
          <FlatList
            // onEndReachedThreshold={0.1}
            // onEndReached={()=> {this.loadding()}}//kéo tới vị trí 0.1 thì load trang mới

            refreshing={refre}
            onRefresh={()=>{this._refre()}}


            data={listProducts}
            renderItem={({ item }) => 
              <View style={products} >
                <View style={productsImage}>
                  <Image style={imageProduct} source={{uri:`${url}${item.images[0]}`}} />
                </View>
                <View style={productsContent}>
                  <Text style={textStyle0}> {item.name.toUpperCase()} </Text>
                  <Text style={textStyle1}> {item.price}$</Text>
                  <Text> {item.material}</Text>
                  <View style={colorStyle}>
                    <Text> Color {item.color}</Text>
                    <View style={{width:12, height:12, backgroundColor: item.color.toLowerCase(), borderRadius:100,}}></View>
                  </View>
                </View>
                <View style={productsShowDetail}>
                  <TouchableOpacity
                    onPress={()=> {this.goDetail(item)}}
                  >
                    <Text style={textStyle2}>SHOW DETAIL</Text>
                  </TouchableOpacity>
                </View>
              </View>
            }    
            keyExtractor={(item)=> item.id.toString()}
              
          />
        </View>
      </View>
    );
  }
}//361*452
function mapStoreToProps(state){
  return {
    listProducts : state.listProducts,
    page: state.page,
    // refre: state.refre,
  }
}

export default  connect(mapStoreToProps,{fetchListProduct,getListProductsThunk,refreshListProduct,loadpage,setRefre})(Product);
var {width, height} = Dimensions.get('window');
const imageWidth=  (width -100)/3;
const imageHeight= (imageWidth /361)*452;
const styles = StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'white',
        margin:width/38,
        borderColor:'gray',
        borderWidth:1,
    },
      header:{
        flex:1,
        flexDirection:'row',
        justifyContent:'space-between',
        alignItems:'center',
        padding:10,
      },
        imagebackstyle:{
          width:width/13,
          height:height/18,
        },
        textheaderstyle:{
          fontSize:width/23,
            color:'#c6386b',
        },
      body:{
        flex:14,
        padding:10,
      },
        products:{
          flexDirection:'row',
          borderColor:'gray',
          borderTopWidth:1,
          paddingTop:10,
          paddingBottom:10,
        },
          productsImage:{
            padding:10,
          },
            imageProduct:{
              width:imageWidth,
              height:imageHeight,
            },
          productsContent:{
            padding:10,
            justifyContent:'space-between',
          },
            textStyle0:{
              color:'gray',
              fontSize:13,
            },
            textStyle1:{
              color:'#c6386b',
            },
                        
          productsShowDetail:{
            padding:10,
            justifyContent:'flex-end',
          },
            textStyle2:{
              color:'#c6386b',
              fontSize:10,
              padding:3,
            },
          colorStyle:{
            flexDirection:'row',
            justifyContent:'space-between',
            alignItems:'center',
          },
          color:{
            width:12, height:12, backgroundColor:'blue',
            borderRadius:100,
          }

})