import React, { Component } from 'react';
import {  StyleSheet,  Text,  View, TouchableOpacity, Dimensions, ImageBackground, im} from 'react-native';
import {connect} from 'react-redux';

import Swiper from 'react-native-swiper';

class Sideproduct extends Component {
  render() {
    const {arrType}= this.props;
    return (
      <View style={styles.banner}>
        <View style={styles.bannerText}>
          <Text style ={styles.text}>LIST OF CATEGORY</Text>
        </View>
        <View style = {styles.bannerImage}>
          <Swiper>
            {arrType.map((e) => (
              <View key={e.id}>
                <TouchableOpacity ref={'Midi'} onPress={()=>this.props.goProduct(e)} >{/* dùng để gán tên cho TouchableOpacity */}
                  <ImageBackground
                    style={styles.image}
                    source={{ uri: `http://192.168.1.111/app/images/type/${e.image}` }}
                  >
                    <Text style={styles.titleText}>{e.name}</Text>
                  </ImageBackground>
                </TouchableOpacity>
              </View>
            ))}
          </Swiper>
        </View>
      </View>
    );
  }
}
function mapStoreToMap(state) {
  return{
    arrType: state.arrType,
  }
}
export default connect(mapStoreToMap)(Sideproduct);
var {width, height} = Dimensions.get('window');
const imageWidth= width -40;
const imageHeight= (imageWidth /933) *456;
const styles = StyleSheet.create({
    banner:{
        height:height*0.37,
        backgroundColor:'#fff',
        margin:width/38,
        marginTop:0,
        shadowColor: 'black',
        shadowOffset: {width: 0, height: height*0.1},
        shadowOpacity: 0.1,
    },
    bannerText:{
      flex:1,
      justifyContent:'center',
      marginLeft:10,
    },
      text:{
        color:'gray',
        fontSize:18,
      },
    bannerImage:{
      flex:4,
      margin:10,
      marginTop:0,      
    },
      titleText:{
        fontSize:18,
        color: 'gray',
      },
      image:{
        width:imageWidth,
        height:imageHeight,
        alignItems:'center',
        justifyContent:'center',
      }
})