import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, Image , Dimensions} from 'react-native';
import {connect} from 'react-redux';

// import avata from '../../Image/temp/profile.png';

class SignOut extends Component {
  
  render() {
    const {container,button, buttonText, imageStyle, profileBody, profileTop, textStyle, textSignIn} = styles;
    return (
        <View>
        <TouchableOpacity
          style={button}
          onPress={()=> {this.props.goSignIn()}}
        >
          <Text style={textSignIn}>SIGN IN</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
function mapStoreToProp(state){
  return {
    user : state.isSignIn,
  }
}
export default  connect(mapStoreToProp)(SignOut);
const {width, height} = Dimensions.get('window');
const imageWidth = width*0.2;
const imageHeight = (imageWidth /2000)*2000;
const styles = StyleSheet.create({
  container: {
    //width:width/1.5,
    flex:1,
    backgroundColor: '#28b08a',
    //  justifyContent: 'center',
      alignItems: 'center',
  },
    imageStyle:{
      width:imageWidth,
      height:imageHeight,
      borderRadius:100,
      margin:20,
    },
    textStyle:{
      color:'white',
      textAlign:'center',
    },
  profileBody:{
    alignItems:'center',
    justifyContent:'center',
    height:height*0.72,
  },
    button:{
      backgroundColor:'white',
      width:(width/1.5)-20,
      height:40,
      margin:5,
      justifyContent:'center',
      borderRadius:10,
      paddingLeft:10,
    },
      buttonText:{
        color:'#28b08a',
      },
      textSignIn:{
        textAlign:'center',
        color:'#28b08a',
      },
})