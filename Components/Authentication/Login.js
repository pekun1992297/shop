import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, TextInput, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import SignIn from "../../Redux/API/SignIn";
import {OnSignIn} from '../../Redux/Reducer/CreateAction';
import saveToken from '../../Redux/API/saveToken';

import back from '../../Image/back_white.png';
import logo from '../../Image/icon-logo.png';

class Login extends Component{
    constructor(props){
        super(props);
        this.state={
            email:'',
            pass:'',
        }
    }

    _login(){
        var {email, pass} = this.state;
        SignIn(email, pass)
        .then(res =>  {
            this.props.OnSignIn(res.user);
            this.props.goBackMenu();
            saveToken(res.token);
        })
        .catch(err => console.log(err, "Dang Nhap khong thanh cong"));
    }

    render() {
        const { container, body, bottom, header, iconStyle, textHeaderStyle, button, buttonLeft, buttontextBottomSignUp, buttonRight, buttontext, buttontextBottom, textInputStyle } = styles
        return (

            <View style={body}>
                <TextInput
                    underlineColorAndroid='transparent'
                    style={textInputStyle}
                    value={this.state.email}
                    onChangeText={(email) => { this.setState({email}) }}
                    placeholder="Enter your email"
                />
                <TextInput
                    underlineColorAndroid='transparent'
                    style={textInputStyle}
                    value={this.state.pass}
                    onChangeText={(pass) => { this.setState({pass}) }}
                    placeholder="Enter your password"
                    secureTextEntry
                />
                <TouchableOpacity
                    style={button}
                    onPress={() => {this._login()}}
                >
                    <Text style={buttontext}>SIGN IN NOW</Text>
                </TouchableOpacity>
            </View>


        );
    }
}

export default  connect(null, {OnSignIn})(Login);

const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#28b08a',
        flex:1,
        padding:width/25,
    },
        header:{
            flex:1,
            flexDirection:'row',
            justifyContent:'space-between',
            marginBottom:20
        },
            iconStyle:{
                width:width/13,
            height:height/18,
            },
            textHeaderStyle:{
                color:'white',
                fontSize:width/16,
            },
        body:{
            flex:7,
            alignItems:'center',
        },
            textInputStyle:{
                backgroundColor:'white',
                width:width-60,
                height:height*0.07,
                borderRadius:15,
                marginTop:20,
            },
            button:{
                borderColor:'white',
                borderWidth:1,
                width:width-60,
                height:height*0.07,
                borderRadius:15,
                alignItems:'center',
                justifyContent:'center',
                backgroundColor:'#28b08a',
                marginTop:20,
            },
            buttontext:{
                color:'white',
            },
        bottom:{
            flex:2,
            alignItems:'center',
            flexDirection:'row',
            justifyContent:'center',
        },
            buttonLeft:{
                width:(width-65)/2,
                height:height*0.07,
                backgroundColor:'white',
                alignItems:'center',
                justifyContent:'center',
                borderBottomLeftRadius:15,
                borderTopLeftRadius:15,
                marginRight:2.5,
            },
            buttonRight:{
                width:(width-65)/2,
                height:height*0.07,
                backgroundColor:'white',
                alignItems:'center',
                justifyContent:'center',
                borderBottomRightRadius:15,
                borderTopRightRadius:15,
                marginLeft:2.5,
            },
                buttontextBottom:{
                    color:'#28b08a',
                },
                buttontextBottomSignUp:{
                    color:'gray',
                },
})