import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity,Alert, TextInput, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import Register from "../../Redux/API/Register";
import {login} from '../../Redux/Reducer/CreateAction';


import back from '../../Image/back_white.png';
import logo from '../../Image/icon-logo.png';

class Registry extends Component{
    constructor(props){
        super(props);
        this.state={
            name:'',
            email:'',
            pass:'',
            repass:'',
            textkq:'',
        }
    }
    gotoSignIn(){
        this.props.login();
    }
    _register(){
        const {name,email,pass} =this.state;
        Register(email,name,pass).then(res=> {
            if(res === 'THANH_CONG') return this._registerSuccess();
            return this._registerFail();
        } )
    }
    _registerSuccess(){
        Alert.alert(
            'Notice',
            'Đăng ký thành công!',
            [
              {text: 'OK', onPress: () => this.gotoSignIn()},
            ],
            { cancelable: false }
          )
    }
    _registerFail(){
        Alert.alert(
            'Notice',
            'Đăng ký không thành công! (Email này đac4 được sử dụng)',
            [
              {text: 'OK', onPress: () => this.setState({email:''})},
            ],
            { cancelable: false }
          )
    }
    render(){
        // const {params} =this.props.navigation.state;
        // const isLogin = params ? params.isRegistry : null;
        const {container, body, bottom, header, iconStyle, textHeaderStyle, button,buttontextBottomSignUp, buttonLeft,buttonRight,buttontext,buttontextBottom,textInputStyle} = styles
        return(
           
                <View style={body}>
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={textInputStyle}
                        value={this.state.name}
                        onChangeText={(name) => { this.setState({name}) }}
                        placeholder="Enter your name"
                    />
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={textInputStyle}
                        value={this.state.email}
                        onChangeText={(email) => { this.setState({email}) }}
                        placeholder="Enter your email"
                    />
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={textInputStyle}
                        value={this.state.pass}
                        onChangeText={(pass) => { this.setState({pass}) }}
                        placeholder="Enter your password"
                        secureTextEntry
                    />
                    <TextInput
                        underlineColorAndroid='transparent'
                        style={textInputStyle}
                        value={this.state.repass}
                        onChangeText={(repass) => { this.setState({repass}) }}
                        placeholder="Re-enter your password"
                        secureTextEntry
                    />
                    <TouchableOpacity
                        style={button}
                        onPress={() => {this._register() }}
                    >
                        <Text style={buttontext}>SIGN UP NOW</Text>
                    </TouchableOpacity>
                    <Text>{this.state.textkq}</Text>
                </View>

               
        );
    }
}
export default  connect(null,{login})(Registry);

const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#28b08a',
        flex:1,
        padding:width/25,
    },
        header:{
            flex:1,
            flexDirection:'row',
            justifyContent:'space-between',
            marginBottom:20
        },
            iconStyle:{
                width:width/13,
            height:height/18,
            },
            textHeaderStyle:{
                color:'white',
                fontSize:width/16,
            },
        body:{
            flex:7,
            alignItems:'center',
        },
            textInputStyle:{
                backgroundColor:'white',
                width:width-60,
                height:height*0.07,
                borderRadius:15,
                marginTop:20,
            },
            button:{
                borderColor:'white',
                borderWidth:1,
                width:width-60,
                height:height*0.07,
                borderRadius:15,
                alignItems:'center',
                justifyContent:'center',
                backgroundColor:'#28b08a',
                marginTop:20,
            },
            buttontext:{
                color:'white',
            },
        bottom:{
            flex:2,
            alignItems:'center',
            flexDirection:'row',
            justifyContent:'center',
        },
            buttonLeft:{
                width:(width-65)/2,
                height:height*0.07,
                backgroundColor:'white',
                alignItems:'center',
                justifyContent:'center',
                borderBottomLeftRadius:15,
                borderTopLeftRadius:15,
                marginRight:2.5,
            },
            buttonRight:{
                width:(width-65)/2,
                height:height*0.07,
                backgroundColor:'white',
                alignItems:'center',
                justifyContent:'center',
                borderBottomRightRadius:15,
                borderTopRightRadius:15,
                marginLeft:2.5,
            },
                buttontextBottom:{
                    color:'#28b08a',
                },
                buttontextBottomSignUp:{
                    color:'gray',
                },
})