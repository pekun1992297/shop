import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity, TextInput, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import Login from './Login';
import Registry from './Registry';
import {login,registry} from '../../Redux/Reducer/CreateAction';

import back from '../../Image/back_white.png';
import logo from '../../Image/icon-logo.png';

class Authentication extends Component{
    // componentDidMount(){
    //     Register('ngoctrang@gmail.com','ngoctrang123','123456').then(res=> console.log(res,"REGISTRY"));
    // }
    goBackMenu() {
        this.props.navigation.navigate('First');
        this.props.navigation.navigate('DrawerOpen');
    }
    // goSignIn(){
    //     this.props.login();
    // }
    // gotoSignIn = {this.goSignIn.bind(this)}
    isLogin(){
        const { isLogin } = this.props;
        if( isLogin === 'REGISTRY') return <Registry/>
        return <Login goBackMenu = {this.goBackMenu.bind(this)}/>
    }
    isFilterLogin(statusname){
        if( statusname === this.props.isLogin) return styles.buttontextBottom;
        return styles.buttontextBottomSignUp;
    }
    render() {
        const { isLogin } = this.props;
        // console.log(isLogin,"LOGIN OR REGISTRY")
        const { container, body, bottom, header, iconStyle, textHeaderStyle, button, buttonLeft, buttontextBottomSignUp, buttonRight, buttontext, buttontextBottom, textInputStyle } = styles
        return (
            <View style={container}>
                <View style={header}>
                    <View>
                        <TouchableOpacity
                            onPress={this.goBackMenu.bind(this)}
                        >
                            <Image style={iconStyle} source={back} />
                        </TouchableOpacity>
                    </View>
                    <View>
                        <Text style={textHeaderStyle}>Wearing a Dress</Text>
                    </View>
                    <View>
                        <Image style={iconStyle} source={logo} />
                    </View>
                </View>

                {this.isLogin()}

                <View style={bottom}>
                    <TouchableOpacity
                        style={buttonLeft}
                        onPress={() => this.props.login() }
                    >
                        <Text style={this.isFilterLogin('LOGIN')}>SIGN IN</Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={buttonRight}
                        onPress={() => this.props.registry() }
                    >
                        <Text style={this.isFilterLogin('REGISTRY')}>SIGN UP</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
function mapStoreToProp(state){
    return{
        isLogin : state.isLogin,
    };
}

export default connect(mapStoreToProp,{login,registry})(Authentication);

const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
    container:{
        backgroundColor:'#28b08a',
        flex:1,
        padding:width/25,
    },
        header:{
            flex:1,
            flexDirection:'row',
            justifyContent:'space-between',
            marginBottom:20
        },
            iconStyle:{
                width:width/13,
            height:height/18,
            },
            textHeaderStyle:{
                color:'white',
                fontSize:width/16,
            },
        body:{
            flex:7,
            alignItems:'center',
        },
            textInputStyle:{
                backgroundColor:'white',
                width:width-60,
                height:height*0.07,
                borderRadius:15,
                marginTop:20,
            },
            button:{
                borderColor:'white',
                borderWidth:1,
                width:width-60,
                height:height*0.07,
                borderRadius:15,
                alignItems:'center',
                justifyContent:'center',
                backgroundColor:'#28b08a',
                marginTop:20,
            },
            buttontext:{
                color:'white',
            },
        bottom:{
            flex:2,
            alignItems:'center',
            flexDirection:'row',
            justifyContent:'center',
        },
            buttonLeft:{
                width:(width-65)/2,
                height:height*0.07,
                backgroundColor:'white',
                alignItems:'center',
                justifyContent:'center',
                borderBottomLeftRadius:15,
                borderTopLeftRadius:15,
                marginRight:2.5,
            },
            buttonRight:{
                width:(width-65)/2,
                height:height*0.07,
                backgroundColor:'white',
                alignItems:'center',
                justifyContent:'center',
                borderBottomRightRadius:15,
                borderTopRightRadius:15,
                marginLeft:2.5,
            },
                buttontextBottom:{
                    color:'#28b08a',
                },
                buttontextBottomSignUp:{
                    color:'gray',
                },
})